# Ansible Role: deployssl

Ansible role to deploy SSL certificates from local PKI to hosts.

## Requirements

No special requirements; note that this role requires root access, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:

    - hosts: database
      roles:
        - role: wreiner.deployssl
          become: yes

## Role Variables

To deploy certificates use the var `deployssl__certificates`:

```
deployssl__certificates:
  - { src: "/some/folder/ca.pem", dest: "/etc/ssl/own-ca.pem", mode: "0644" }
```

This role only sets initial permissions, to change ownership of the deployed files use another role.
